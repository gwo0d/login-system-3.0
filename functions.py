try:
  import bcrypt
  import secrets as s
except ImportError:
  print("\nNecessary libraries unavailable! Please try again...")

def signUp(username, password):
  try:
    # Read in stored usernames to array
    storedUser = []

    with open("user.db", "rt") as userFile:
      for line in userFile:
        storedUser.append(line.encode("utf-8").strip(b'\n'))
      userFile.close()

    userFile = open("user.db", "ab")
    passFile = open("pass.db", "ab")

    # Check if username is available
    available = True

    for i in range(len(storedUser)):
      if bcrypt.checkpw(username.encode("utf-8"), storedUser[i]):
        available = False
    
    # If the username is available, write to database
    if available:
      uToStore = bcrypt.hashpw(username.encode("utf-8"), bcrypt.gensalt(rounds = 8))
      pToStore = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt(rounds = 16))
        
      userFile.write(uToStore)
      userFile.write(b'\n')

      passFile.write(pToStore)
      passFile.write(b'\n')

      userFile.close()
      passFile.close()

      return "Credentials stored!"
    else:
      return "Username unavailable. Please try again..."
  except:
    return "Error! Please try again..."

def login(username, password):
  try:
    storedUser = []
    storedPass = []

    with open("user.db", "rt") as userFile:
      for line in userFile:
        storedUser.append(line.encode("utf-8").strip(b'\n'))
      userFile.close()

    with open("pass.db", "rt") as passFile:
      for line in passFile:
        storedPass.append(line.encode("utf-8").strip(b'\n'))
      passFile.close()

      location = 0
      unFound = False
      pwFound = False

      for i in range(len(storedUser)):
        if bcrypt.checkpw(username.encode("utf-8"), storedUser[i]):
          unFound = True
          location = i
          break

      if bcrypt.checkpw(password.encode("utf-8"), storedPass[location]):
        pwFound = True
      
      if unFound == True and pwFound == True:
        return "Logged in!"
      else:
        return "Credentials not found!"
  except:
    return "Error! Please try again..."

def randomArtStr(x):  # Returns a strong random string of length "x" of characters selected from "dictionary"
  try:
    dictionary = " @"
    string = ""
    for i in range(x):
      string += s.choice(dictionary)
    return string
  except:
    print("Error... please try again.\n")

def randomStr(x):  # Returns a strong random string of length "x" of characters selected from "dictionary"
  try:
    dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-+=@#~!£$%&*"
    string = ""
    for i in range(x):
      string += s.choice(dictionary)
    return string
  except:
    print("Error... please try again.\n")