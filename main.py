try:
  from functions import *
except ImportError:
  print("\nNecessary libraries unavailable! Please try again...")

go = True

while go:
  try:
    option = int(input("•Enter 1 to login\n•Enter 2 to sign up\n•Enter 3 to genarate random art\n•Enter 4 to genarate a random password\n•Enter 5 to exit\n: "))

    if option == 1:
      username = input("\nEnter your username\n: " )
      password = input("\nEnter your password\n: ")
              
      print("\n" + login(username, password) + "\n")

    elif option == 2:
      username = input("\nEnter a username\n: ")
      password = input("\nEnter a password\n: ")

      print("\n" + signUp(username, password) + "\n")
          
    elif option == 3:
      print("\n-------------------------")
      for i in range(10):
        randomness = randomArtStr(23)
        print("|" + randomness + "|")
      print("-------------------------\n")
          
    elif option == 4:
      length = int(input("\nEnter the length of your password\n: "))
      print("\nYour password is: " + randomStr(length) + "\n")
          
    elif option == 5:
      go = False
  except:
    print("Error! Please try again...")